# Postlink

## Purpose
Posts a link to saved.io. The list is a command line parameter.

## Libraries Used

 - requests
 - pyperclip
 - python-dotenv

## Notes

Used [this](https://stackoverflow.com/questions/51233/how-can-i-retrieve-the-page-title-of-a-webpage-using-python) to remove the BeautifulSoup dependency. Just a big dependency to get the title of the page.