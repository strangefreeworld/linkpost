import os
# from pathlib import Path

def find_env_file() -> str:
    package_local = __file__.replace(os.path.basename(__file__), ".env")
    if os.path.exists(package_local):
        return package_local
    lp_specific = os.path.expanduser("~/.lpenv")
    if os.path.exists(lp_specific):
        return lp_specific
    generic_env = os.path.expanduser("~/.env")
    if os.path.exists(generic_env):
        return generic_env
    return ""