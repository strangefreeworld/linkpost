#!/usr/bin/env python3

import os.path
import json
import requests
from dotenv import load_dotenv

SAVEDIO_API_URL = "http://devapi.saved.io/bookmarks/"


def get_html_text(article_url):
    headers = {"Accept-Language": "en-US, en;q=0.5", 'User-agent': 'Mozilla/5.0'}
    result = requests.get(article_url, headers=headers)
    return result.text.replace('\xa0', '')

def get_title(html_text):
    start_title = html_text.find("<title")
    end_title = html_text.find(">", start_title)
    return html_text[end_title + 1: html_text.find("</title>", end_title)]

if __name__ == "__main__":
    load_dotenv(os.path.expanduser("~/.env"))
    with open("links.txt") as link_file:
        link_data = link_file.readlines()
    for line in link_data:
        elements = line.split(" ")
        clean_url = elements[0].split("?")[0]
        page_title = get_title(get_html_text(clean_url))
        params = {
            "devkey": os.environ["DEV_KEY"],
            "key" : os.environ["API_KEY"],
            "url" : clean_url,
            "title": page_title,
            "list": elements[1][:-1]
        }
        response = requests.post(SAVEDIO_API_URL, params=params)
        print(response.json())