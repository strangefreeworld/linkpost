#!/usr/bin/env python3

import os.path
import json
import requests
from dotenv import load_dotenv

SAVEDIO_API_URL = "http://devapi.saved.io/bookmarks/"


def get_html_text(article_url):
    headers = {"Accept-Language": "en-US, en;q=0.5", 'User-agent': 'Mozilla/5.0'}
    result = requests.get(article_url, headers=headers)
    return result.text.replace('\xa0', '')

def get_title(html_text):
    start_title = html_text.find("<title")
    end_title = html_text.find(">", start_title)
    return html_text[end_title + 1: html_text.find("</title>", end_title)]
    

def process_entry(bookmark_entry):
    if bookmark_entry["bk_title"].startswith("ype html"):
        new_url = bookmark_entry["bk_url"].replace("\\", "").split("?")[0]
        return new_url
    return None


if __name__ == "__main__":
    load_dotenv(os.path.expanduser("~/.env"))
    with open("links.json") as json_file:
        bookmark_data = json.loads(json_file.read())
    # for bookmark in bookmark_data:
    #    process_entry(bookmark)
    bad_links = list(filter(lambda item: item is not None, map(process_entry, bookmark_data)))
    with open("links.txt", "w") as linkfile:
        for item in bad_links:
            linkfile.write(f"{item}\n")