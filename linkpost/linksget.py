#!/usr/bin/env python3

import os.path
import pyperclip
import requests
from dotenv import load_dotenv

if __name__ == "__main__":
    load_dotenv(os.path.expanduser("~/.env"))
    bkid = pyperclip.paste()
    params = {
        "devkey": os.environ["DEV_KEY"],
        "key" : os.environ["API_KEY"],
        "limit": 1000,
    }
    print(str(params))
    response = requests.get("http://devapi.saved.io/bookmarks/", params=params)
    with open('links.json', 'w') as linkfile:
        linkfile.write(response.text)
    #print(response.json())
