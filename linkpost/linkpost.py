import os
import click
import pyperclip
import requests
from dotenv import load_dotenv
from find_env import find_env_file


def article_html(article_url):
    """
    This creates HTTP headers and gets the recipe page. Some sites needed this
    to respond.
    :param url_recipe: URL of recipe
    :return: Text of the HTML page
    """
    headers = {"Accept-Language": "en-US, en;q=0.5", 'User-agent': 'Mozilla/5.0'}
    result = requests.get(article_url, headers=headers)
    return result.text.replace('\xa0', '')

def get_title(html_text):
    start_title = html_text.find("<title")
    end_title = html_text.find(">", start_title)
    return html_text[end_title + 1: html_text.find("</title>", end_title)]

@click.command()
@click.argument("folder")
def post_link(folder):
    env_file = find_env_file()#__file__.replace(os.path.basename(__file__), ".env")
    if not len(env_file):
        click.echo("No environment file found; aborting...")
        return
    load_dotenv(env_file)
    headers = {"Accept-Language": "en-US, en;q=0.5", 'User-agent': 'Mozilla/5.0', 'content-type': 'application/json'}
    url = pyperclip.paste()
    clean_url = url.split('?')[0]
    html = article_html(clean_url)
    page_title = get_title(html)
    params = {
        "devkey": os.environ["DEV_KEY"],
        "key" : os.environ["API_KEY"],
        "url" : clean_url,
        "title": page_title,
        "list": folder
        }
    response = requests.post("http://devapi.saved.io/bookmarks/", params=params)
    print(response.json())
