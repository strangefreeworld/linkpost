#!/usr/bin/env python3

import os.path
import pyperclip
import requests
import click
from dotenv import load_dotenv
from find_env import find_env_file

@click.command()
def delete_link():
    env_file = find_env_file()
    if not len(env_file):
        click.echo("No environment file found; aborting...")
        return
    load_dotenv(env_file)
    bkid = pyperclip.paste()
    params = {
        "devkey": os.environ["DEV_KEY"],
        "key" : os.environ["API_KEY"],
        "id" : bkid,
    }
    print(str(params))
    response = requests.delete("http://devapi.saved.io/bookmarks/", params=params)
    #print(response.json())
